package Sudoku.Sudoku;

import Sudoku.Sudoku.Exceptions.BadValueException;
import Sudoku.Sudoku.Exceptions.ColumnIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.RowIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.UnauthorizedCharacterException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class TestAjoutValeur9 {

    @Test
    public void testAjoutValeurGrille() {
        Grille9 grille = new Grille9();
        try {
            grille.setValue(1, 1, '1');
        } catch (BadValueException | RowIndexOutOfBoundsException |
                ColumnIndexOutOfBoundsException | UnauthorizedCharacterException e) {
            e.printStackTrace();
        }

        try {
            assertEquals('1', grille.getValue(1, 1));
        } catch (RowIndexOutOfBoundsException | ColumnIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testAjoutValeurInadequate() throws RowIndexOutOfBoundsException, ColumnIndexOutOfBoundsException, BadValueException {
        Grille9 grille = new Grille9();
        try {
            grille.setValue(1, 1, 'R');
        } catch (UnauthorizedCharacterException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAjoutValeurXHorsGrille() throws ColumnIndexOutOfBoundsException, BadValueException, UnauthorizedCharacterException {
        Grille9 grille = new Grille9();
        try {
            grille.setValue(99, 1, '1');
            fail("Le x est trop grand");
        } catch (RowIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAjoutValeurYLimiteCorrecteGrille() throws RowIndexOutOfBoundsException, BadValueException, UnauthorizedCharacterException {
        Grille9 grille = new Grille9();
        try {
            grille.setValue(0, 8, '1');
        } catch (ColumnIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        try {
            assertEquals('1', grille.getValue(0, 8));
        } catch (RowIndexOutOfBoundsException | ColumnIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAjoutValeurYLimiteIncorrecteGrille() throws RowIndexOutOfBoundsException, BadValueException, UnauthorizedCharacterException {
        Grille9 grille = new Grille9();
        try {
            grille.setValue(0, 9, '1');
            fail("la colonnes est censée être hors grille");
        } catch (ColumnIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAjoutValeurYHorsGrille() throws RowIndexOutOfBoundsException, BadValueException, UnauthorizedCharacterException {
        Grille9 grille = new Grille9();
        try {
            grille.setValue(1, 99, '1');
            fail("Le y est trop grand");
        } catch (ColumnIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAjoutValeurXLimiteCorrecteGrille() throws UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException {
        Grille9 grille = new Grille9();
        try {
            grille.setValue(8, 0, '1');
        } catch (RowIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        try {
            assertEquals('1', grille.getValue(8, 0));
        } catch (RowIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAjoutValeurXLimiteIncorrecteGrille() throws UnauthorizedCharacterException, BadValueException, ColumnIndexOutOfBoundsException {
        Grille9 grille = new Grille9();
        try {
            grille.setValue(9, 0, '1');
            fail("Le x ou y hors limite");
        } catch (RowIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

}

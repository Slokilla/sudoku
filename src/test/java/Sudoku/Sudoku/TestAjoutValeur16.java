package Sudoku.Sudoku;

import Sudoku.Sudoku.Exceptions.BadValueException;
import Sudoku.Sudoku.Exceptions.ColumnIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.RowIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.UnauthorizedCharacterException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class TestAjoutValeur16 {

    @Test
    public void testAjoutValeurGrille16() {
        Grille16 grille = new Grille16();
        try {
            grille.setValue(1, 1, '1');
        } catch (BadValueException | ColumnIndexOutOfBoundsException |
                UnauthorizedCharacterException | RowIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        try {
            assertEquals('1', grille.getValue(1, 1));
        } catch (RowIndexOutOfBoundsException | ColumnIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testAjoutValeurInadequate16() throws RowIndexOutOfBoundsException, BadValueException, ColumnIndexOutOfBoundsException {
        Grille16 grille = new Grille16();
        try {
            grille.setValue(1, 1, 'R');
            fail("La valeur est illégale");
        } catch (UnauthorizedCharacterException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAjoutValeurXHorsGrilleHaut16() throws UnauthorizedCharacterException, BadValueException, ColumnIndexOutOfBoundsException {
        Grille16 grille = new Grille16();
        try {
            grille.setValue(99, 1, '1');
            fail("Le x est trop grand");
        } catch (RowIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAjoutValeurXHorsGrilleBas16() throws UnauthorizedCharacterException, BadValueException, ColumnIndexOutOfBoundsException {
        Grille16 grille = new Grille16();
        try {
            grille.setValue(-100, 1, '1');
            fail("Le x est trop petit");
        } catch (RowIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAjoutValeurXLimiteCorrecteGrille16() throws UnauthorizedCharacterException, BadValueException, ColumnIndexOutOfBoundsException {
        Grille16 grille = new Grille16();
        try {
            grille.setValue(0, 15, '1');
        } catch (RowIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        try {
            assertEquals('1', grille.getValue(0, 15));
        } catch (RowIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAjoutValeurXLimiteIncorrecteGrille16() throws UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException {
        Grille16 grille = new Grille16();
        try {
            grille.setValue(16, 0, '1');
            fail("Le x ou y hors limite");
        } catch (RowIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAjoutValeurYHorsGrilleHaut16() throws UnauthorizedCharacterException, RowIndexOutOfBoundsException, BadValueException {
        Grille16 grille = new Grille16();
        try {
            grille.setValue(1, 99, '1');
            fail("Le y est trop grand");
        } catch (ColumnIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAjoutValeurYHorsGrilleBas16() throws UnauthorizedCharacterException, RowIndexOutOfBoundsException, BadValueException {
        Grille16 grille = new Grille16();
        try {
            grille.setValue(1, -99, '1');
            fail("Le y est trop grand");
        } catch (ColumnIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAjoutValeurYLimiteCorrecteGrille16() throws UnauthorizedCharacterException, RowIndexOutOfBoundsException, BadValueException {
        Grille16 grille = new Grille16();
        try {
            grille.setValue(15, 0, '1');
        } catch (ColumnIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        try {
            assertEquals('1', grille.getValue(15, 0));
        } catch (ColumnIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAjoutValeurYLimiteIncorrecteGrille16() throws UnauthorizedCharacterException, RowIndexOutOfBoundsException, BadValueException {
        Grille16 grille = new Grille16();
        try {
            grille.setValue(0, 16, '1');
            fail("Le x ou y hors limite");
        } catch (ColumnIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }
}

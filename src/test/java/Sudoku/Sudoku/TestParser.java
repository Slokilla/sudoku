package Sudoku.Sudoku;

import Sudoku.Sudoku.Exceptions.BadValueException;
import Sudoku.Sudoku.Exceptions.ColumnIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.RowIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.UnauthorizedCharacterException;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.fail;

public class TestParser {

    @Test
    public void testParseur9() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException {
        try {
            Grille g = new Grille9();
            Parser.parse("ressources/sudoku1.txt", g);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParseur9Incomplet() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException {
        try {
            Grille g = new Grille9();
            Parser.parse("ressources/MauvaiseGrille9.txt", g);
            fail("Le parseur n'est pas censé fonctionner");
        } catch (IOException | UnauthorizedCharacterException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParseurMauvaisFormat() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException {
        try {
            Grille g = new Grille9();
            Parser.parse("ressources/GrilleMauvaisFormat.txt", g);
            fail("Le parseur n'est pas censé fonctionner");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParseur16() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException {
        try {
            Grille g = new Grille16();
            Parser.parse("ressources/GrilleTest16.txt", g);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParseur16Incomplet() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException {
        try {
            Grille g = new Grille16();
            Parser.parse("ressources/MauvaiseGrille16.txt", g);
            fail("Le parseur n'est pas censé fonctionner");
        } catch (IOException | UnauthorizedCharacterException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParseurFichierInexistant() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException {
        try {
            Grille g = new Grille9();
            Parser.parse("ressources/GrilleInexistante.txt", g);
            fail("Le grille n'est pas censée être trouvée");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

package Sudoku.Sudoku;

import Sudoku.Sudoku.Exceptions.BadValueException;
import Sudoku.Sudoku.Exceptions.ColumnIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.RowIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.UnauthorizedCharacterException;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class TestGrille {

    @Test
    public void testGrilleComplete() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException, IOException {
        Grille g = new Grille9();
        Parser.parse("ressources/sudoku3.txt", g);
        assertTrue(g.complete());
    }

    @Test
    public void testGrilleIncomplete() throws UnauthorizedCharacterException, RowIndexOutOfBoundsException, BadValueException, ColumnIndexOutOfBoundsException {
        Grille9 grille = new Grille9();

        for (int i = 0; i < grille.getDimension(); i++) {
            for (int j = 0; j < grille.getDimension(); j++) {
                grille.setValue(i, j, '@');
            }
        }

        /*try {
            grille.setValue(8,8,'@');
        } catch (BadValueException e) {
            e.printStackTrace();
        } catch (RowIndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch (ColumnIndexOutOfBoundsException e) {
            e.printStackTrace();
        }*/

        assertFalse(grille.complete());
    }

    @Test
    public void testGrilleComplete16() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException, IOException {
        Grille g = new Grille16();
        Parser.parse("ressources/GrilleTest16.txt", g);
        assertTrue(g.complete());
    }

    @Test
    public void testGrilleIncomplete16() throws ColumnIndexOutOfBoundsException, UnauthorizedCharacterException, RowIndexOutOfBoundsException, BadValueException {
        IGrille grille = new Grille16();
        for (int i = 0; i < grille.getDimension(); i++) {
            for (int j = 0; j < grille.getDimension(); j++) {
                grille.setValue(i, j, '@');
            }
        }

        assertFalse(grille.complete());
    }

    @Test
    public void testRentrerMemeValeurSurLigne() throws ColumnIndexOutOfBoundsException, UnauthorizedCharacterException, RowIndexOutOfBoundsException, BadValueException {
        IGrille grille = new Grille16();

        for (int i = 0; i < grille.getDimension(); i++) {
            for (int j = 0; j < grille.getDimension(); j++) {
                grille.setValue(i, j, '@');
            }
        }

        grille.setValue(1, 1, '1');

        assertFalse(grille.isPossible(1, 2, '1'));
    }

    @Test
    public void testRentrerMemeValeurSurColonne() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException {
        IGrille grille = new Grille16();

        for (int i = 0; i < grille.getDimension(); i++) {
            for (int j = 0; j < grille.getDimension(); j++) {
                grille.setValue(i, j, '@');
            }
        }
        grille.setValue(1, 1, '1');
        assertFalse(grille.isPossible(2, 1, '1'));
    }

    @Test
    public void testRentrerMemeValeurSurCarre() throws ColumnIndexOutOfBoundsException, UnauthorizedCharacterException, RowIndexOutOfBoundsException, BadValueException {
        IGrille grille = new Grille16();

        for (int i = 0; i < grille.getDimension(); i++) {
            for (int j = 0; j < grille.getDimension(); j++) {
                grille.setValue(i, j, '@');
            }
        }

        grille.setValue(1, 1, '1');

        assertFalse(grille.isPossible(2, 2, '1'));
    }

    @Test
    public void testRentrerMemeValeurSurCarreProches() throws ColumnIndexOutOfBoundsException, UnauthorizedCharacterException, RowIndexOutOfBoundsException, BadValueException {
        IGrille grille = new Grille16();

        for (int i = 0; i < grille.getDimension(); i++) {
            for (int j = 0; j < grille.getDimension(); j++) {
                grille.setValue(i, j, '@');
            }
        }

        grille.setValue(3, 3, '1');
        assertTrue(grille.isPossible(4, 4, '1'));
    }

    @Test
    public void testLeverExceptionForcerValeurNonPossible() throws UnauthorizedCharacterException, RowIndexOutOfBoundsException, BadValueException, ColumnIndexOutOfBoundsException {
        IGrille grille = new Grille16();

        for (int i = 0; i < grille.getDimension(); i++) {
            for (int j = 0; j < grille.getDimension(); j++) {
                grille.setValue(i, j, '@');
            }
        }

        grille.setValue(1, 1, '1');
        try {
            grille.setValue(1, 2, '1');
            fail("La valeur aurait du lever une exception (valeur incorrecte)");
        } catch (BadValueException e) {
            e.printStackTrace();
        }
    }

}

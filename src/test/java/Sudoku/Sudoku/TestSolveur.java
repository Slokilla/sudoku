package Sudoku.Sudoku;


import Sudoku.Sudoku.Exceptions.BadValueException;
import Sudoku.Sudoku.Exceptions.ColumnIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.RowIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.UnauthorizedCharacterException;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class TestSolveur {
    @Test
    public void testSolve16() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException, IOException {
        Grille g = new Grille16();
        Parser.parse("ressources/sudoku4.txt", g);

        Solveur.solve(g);

        assertTrue(g.complete());
    }

    @Test
    public void testSolve16_Dure() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException, IOException {
        Grille g = new Grille16();
        Parser.parse("ressources/grilleDodo.txt", g);

        Solveur.solve(g);

        assertTrue(g.complete());
    }

    @Test
    public void testSolve16_Expert() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException, IOException {
        Grille g = new Grille16();
        Parser.parse("ressources/Grille16Expert.txt", g);

        Solveur.solve(g);

        assertTrue(g.complete());
    }

    @Test
    public void testSolve9_1() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException, IOException {
        Grille g = new Grille9();
        Parser.parse("ressources/sudoku1.txt", g);

        Solveur.solve(g);

        assertTrue(g.complete());
    }

    @Test
    public void testSolve9_2() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException, IOException {
        Grille g = new Grille9();
        Parser.parse("ressources/sudoku2.txt", g);

        Solveur.solve(g);

        assertTrue(g.complete());
    }

    @Test
    public void testSolve9Dure() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException, IOException {
        Grille g = new Grille9();
        Parser.parse("ressources/Grille9Dure.txt", g);

        Solveur.solve(g);

        assertTrue(g.complete());
    }

    @Test
    public void testSolve9DiaboliqueOfDoom() throws RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, BadValueException, IOException {
        Grille g = new Grille9();
        Parser.parse("ressources/Grille9DiaboliqueOfDoom.txt", g);

        Solveur.solve(g);

        assertTrue(g.complete());
    }

    @Test
    public void testPossibilites() throws RowIndexOutOfBoundsException, ColumnIndexOutOfBoundsException, BadValueException, IOException, UnauthorizedCharacterException {
        Grille g = new Grille9();
        Parser.parse("ressources/sudoku1.txt", g);

        g.getPlusSur();

        assertEquals(5, g.getPossibilites()[0][1]);
    }

    @Test
    public void testPlusSur() throws BadValueException, RowIndexOutOfBoundsException, UnauthorizedCharacterException, ColumnIndexOutOfBoundsException, IOException {
        Grille g = new Grille9();
        Parser.parse("ressources/sudoku1.txt", g);

        g.getPlusSur();

        assertArrayEquals(new int[]{8, 4}, g.getPlusSur());
    }
}

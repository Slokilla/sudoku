package Sudoku.Sudoku;

/**
 * Implementation d'une grille.
 */
public class Grille9 extends Grille {

    /**
     * Constructeur de la grille 9x9.
     */
    public Grille9() {
        super(new char[]{
                '1', '2', '3',
                '4', '5', '6',
                '7', '8', '9'
        });
    }
}

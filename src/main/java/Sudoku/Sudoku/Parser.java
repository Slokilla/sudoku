package Sudoku.Sudoku;

import Sudoku.Sudoku.Exceptions.BadValueException;
import Sudoku.Sudoku.Exceptions.ColumnIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.RowIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.UnauthorizedCharacterException;

import java.io.InputStream;
import java.io.IOException;
import java.io.EOFException;
import java.io.Reader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * Parser permettant de récupérer la grille à partir d'un fichier texte.
 */
public final class Parser {

    /**
     * Constructeur par défaut privé
     * puisque Parser est utilitaire.
     */
    private Parser() {
    }

    /**
     * Parser de la grille, il remplit un objet grille
     * à partir d'un fichier texte.
     *
     * @param in le fichier contenant le grille
     * @param grille l'objet grille à remplir
     * @throws IOException Si le fichier n'est pas existant
     * @throws UnauthorizedCharacterException si on essaie de
     * rentrer un caractère non autorisé par la dimension
     * de la grille
     * @throws RowIndexOutOfBoundsException si on essaie d'écrire sur une
     * ligne inexistante
     * @throws BadValueException si la valeur est incompatible
     * avec les autres de la grille
     * @throws ColumnIndexOutOfBoundsException si on essaie d'écrire sur une
     * colonne inexistante
     */
    public static void parse(final InputStream in, final Grille grille)
    throws IOException,
    UnauthorizedCharacterException,
    RowIndexOutOfBoundsException,
    BadValueException,
    ColumnIndexOutOfBoundsException {
        Reader reader = null;
        try {
            reader = new InputStreamReader(in, StandardCharsets.UTF_8);

            int dimension = grille.getDimension();
            char[] buffer = new char[dimension];
            for (int line = 0; line < dimension; line++) {
                int lus = reader.read(buffer);
                if (lus != dimension) {
                    throw new EOFException("format incorrect");
                }
                for (int i = 0; i < dimension; i++) {
                    grille.setValue(line, i, buffer[i]);
                }
                lus = reader.read(new char[1]);
                if (lus != 1) {
                    throw new EOFException("pas de fin de ligne? l=" + line);
                }
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        reader.close();
    }

    /**
     * @param s la chaîne à parse
     * @param grille la grille recevant le parse
     * @throws IOException Si le fichier n'est pas existant
     * @throws ColumnIndexOutOfBoundsException si on essaie d'écrire sur une
     * colonne non existante
     * @throws UnauthorizedCharacterException si on essaie de
     * rentrer un caractère non autorisé par la dimension de la grille
     * @throws RowIndexOutOfBoundsException si on essaie d'écrire sur une
     * ligne non existante
     * @throws BadValueException si la valeur est incompatible avec les autres
     * la grille
     */
    public static void parse(final String s, final Grille grille)
    throws IOException,
    ColumnIndexOutOfBoundsException,
    UnauthorizedCharacterException,
    RowIndexOutOfBoundsException,
    BadValueException {
        parse(new FileInputStream(s), grille);
    }

}

package Sudoku.Sudoku;

/**
 * Implementation d'une grille.
 */
public class Grille16 extends Grille {

    /**
     * Constructeur de grille 16.
     */
    public Grille16() {
        super(new char[]{
                '1', '2', '3', '4',
                '5', '6', '7', '8',
                '9', '0', 'a', 'b',
                'c', 'd', 'e', 'f'
        });
    }
}

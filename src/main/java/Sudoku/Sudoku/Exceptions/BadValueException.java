package Sudoku.Sudoku.Exceptions;

/**
 * Exception levée si une valeur rentrée n'est pas correcte.
 */
public class BadValueException extends Exception {

    /**
     * Constructeur par défaut.
     */
    public BadValueException() {
        super();
    }

    /**
     * Constructeur à message.
     * @param message le message à afficher
     */
    public BadValueException(final String message) {
        super(message);
    }

    /**
     * Constructeur "Poupée russe".
     * @param message le message à afficher
     * @param cause l'erreur précédant.
     */
    public BadValueException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

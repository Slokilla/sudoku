package Sudoku.Sudoku.Exceptions;

/**
 * Classe exception hors ligne.
 */
public class RowIndexOutOfBoundsException extends Exception {

    /**
     * Constructeur par défaut.
     */
    public RowIndexOutOfBoundsException() {
        super();
    }

    /**
     * Constructeur à message.
     * @param message le message à afficher
     */
    public RowIndexOutOfBoundsException(final String message) {
        super(message);
    }

    /**
     * Constructeur "Poupée russe".
     * @param message le message à afficher
     * @param cause l'erreur précédant.
     */
    public RowIndexOutOfBoundsException(final String message,
    final Throwable cause) {
        super(message, cause);
    }
}

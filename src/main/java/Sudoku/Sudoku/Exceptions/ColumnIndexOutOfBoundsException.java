package Sudoku.Sudoku.Exceptions;

/**
 * Classe exception hors colonne.
 */
public class ColumnIndexOutOfBoundsException extends Exception {

    /**
     * Constructeur par défaut.
     */
    public ColumnIndexOutOfBoundsException() {
        super();
    }

    /**
     * Constructeur à message.
     * @param message le message à afficher
     */
    public ColumnIndexOutOfBoundsException(final String message) {
        super(message);
    }

    /**
     * Constructeur "Poupée russe".
     * @param message le message à afficher
     * @param cause l'erreur précédant.
     */
    public ColumnIndexOutOfBoundsException(final String message,
    final Throwable cause) {
        super(message, cause);
    }
}

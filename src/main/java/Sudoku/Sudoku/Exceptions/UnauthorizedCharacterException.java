package Sudoku.Sudoku.Exceptions;

/**
 * Classe exception caractere non autorise.
 */
public class UnauthorizedCharacterException extends Exception {

    /**
     * Constructeur par défaut.
     */
    public UnauthorizedCharacterException() {
        super();
    }

    /**
     * Constructeur à message.
     * @param message le message à afficher
     */
    public UnauthorizedCharacterException(final String message) {
        super(message);
    }

    /**
     * Constructeur "Poupée russe".
     * @param message le message à afficher
     * @param cause l'erreur précédant.
     */
    public UnauthorizedCharacterException(final String message,
    final Throwable cause) {
        super(message, cause);
    }
}

package Sudoku.Sudoku;

import Sudoku.Sudoku.Exceptions.RowIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.ColumnIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.UnauthorizedCharacterException;
import Sudoku.Sudoku.Exceptions.BadValueException;

/**
 * Interface de grille.
 */
public interface IGrille {

    /**
     * Caractère interprété comme vide.
     */
    char EMPTY = '@';

    /**
     * Getter de dimension.
     * @return largeur/hauteur de la grille
     */
    int getDimension();

    /**
     * Vérifie si la grille est complete.
     * @return true si la grille est complete
     */
    boolean complete();

    /**
     * Vérifie si la valeur est possible à une case donnée.
     * @param x l'abcisse
     * @param y l'ordonnée
     * @param value la valeur à rentrer
     * @return true si c'est possible
     */
    boolean isPossible(int x, int y, char value);

    /**
     * Rentre une valeur dans un tableau.
     * @param x l'abcisse
     * @param y l'ordonnée
     * @param value la valeur à rentrer
     * @throws RowIndexOutOfBoundsException si x est hors bornes (0-8)
     * @throws ColumnIndexOutOfBoundsException si y est hors bornes (0-8)
     * @throws BadValueException si la valeur est interdite aux vues des
     * autres valeurs de la grille
     * @throws UnauthorizedCharacterException si value n'est pas un caractere
     * autorisé
     */
    void setValue(int x, int y, char value) throws BadValueException,
    RowIndexOutOfBoundsException, ColumnIndexOutOfBoundsException,
    UnauthorizedCharacterException;

    /**
     * Recupère une valeur de la grille.
     * @param x abcisse dans la grille
     * @param y ordonnée dans la grille
     * @return valeur dans la case x,y
     * @throws RowIndexOutOfBoundsException si la ligne est hors-grille
     * @throws ColumnIndexOutOfBoundsException si la colonne est hors grille
     */
    char getValue(int x, int y) throws RowIndexOutOfBoundsException,
    ColumnIndexOutOfBoundsException;
}

package Sudoku.Sudoku;


import Sudoku.Sudoku.Exceptions.BadValueException;
import Sudoku.Sudoku.Exceptions.ColumnIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.RowIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.UnauthorizedCharacterException;

public final class Solveur {

    /**
     * constructeur privé pour ne pas instancier la classe.
     */
    private Solveur() {

    }

    /**
     * Solveur du sudoku.
     * <p>
     * Dans un premier temps, on remplis les cases par déduction directe (quand on a qu'une
     * possibilité). Ensuite, si il n'y a plus aucune case avec une seule possibilité,
     * on essaie la plus petite valide.
     * On reprend le remplissage par déduction. Si on arrive à un moment
     * auquel on n'a plus aucune valeur possible dans aucune case, on backtrack.
     * On annule tout les changement jusqu'au dernier moment auquel on a eu le choix.
     * On prend à nouveau la plus petite valeur possible à partir de la dernière essayée.
     *
     * @param g la grille à résoudre
     * @param x la ligne de la première case avec le moins de possibilités
     * @param y la colonne de la case avec le moins de possibilités
     * @return un boolean pour pratiquer la récursivité
     * @throws RowIndexOutOfBoundsException    voir getValue()
     * @throws ColumnIndexOutOfBoundsException voir getValue()
     * @throws UnauthorizedCharacterException  voir getValue()
     * @throws BadValueException               voir getValue()
     */
    private static boolean solve(final Grille g, final int x, final int y) throws RowIndexOutOfBoundsException, ColumnIndexOutOfBoundsException, UnauthorizedCharacterException, BadValueException {
        if (g.getSeuleValeurPossibleSurUneCase().length != 0) {
            char[] valAt = g.getSeuleValeurPossibleSurUneCase();
            g.setValue(valAt[0], valAt[1], valAt[2]);
            int[] plusSur = g.getPlusSur();

            if (solve(g, plusSur[0], plusSur[1])) {
                return true;
            }
            g.setValue(valAt[0], valAt[1], g.EMPTY);
            return false;

        } else if (g.getSeulEmplacementPossibleSurLaLignePourUneValeur().length != 0) {
            char[] valAt = g.getSeulEmplacementPossibleSurLaLignePourUneValeur();
            g.setValue(valAt[0], valAt[1], valAt[2]);

            int[] plusSur = g.getPlusSur();
            if (solve(g, plusSur[0], plusSur[1])) {
                return true;
            }
            g.setValue(valAt[0], valAt[1], g.EMPTY);
            return false;

        } else if (g.getSeulEmplacementPossibleSurLaColonnePourUneValeur().length != 0) {
            char[] valAt = g.getSeulEmplacementPossibleSurLaColonnePourUneValeur();
            g.setValue(valAt[0], valAt[1], valAt[2]);

            int[] plusSur = g.getPlusSur();
            if (solve(g, plusSur[0], plusSur[1])) {
                return true;
            }
            g.setValue(valAt[0], valAt[1], g.EMPTY);
            return false;

        } else if (g.getSeulEmplacementPossibleSurLeBlocPourUneValeur().length != 0) {
            char[] valAt = g.getSeulEmplacementPossibleSurLeBlocPourUneValeur();
            g.setValue(valAt[0], valAt[1], valAt[2]);

            int[] plusSur = g.getPlusSur();
            if (solve(g, plusSur[0], plusSur[1])) {
                return true;
            }
            g.setValue(valAt[0], valAt[1], g.EMPTY);
            return false;

        } else {
            if (g.getPossibilites()[x][y] != 0) {
                for (int i = 0; i < g.getPossibilites()[x][y]; i++) {
                    if (g.getValue(x, y) == g.EMPTY) {
                        for (char c : g.getLegalChars()) {
                            if (g.isPossible(x, y, c)) {
                                g.setValue(x, y, c);
                                int[] plusSur = g.getPlusSur();
                                if (solve(g, plusSur[0], plusSur[1])) {
                                    return true;
                                }
                            }
                            g.getPlusSur();
                            g.setValue(x, y, g.EMPTY);
                        }
                        return false;
                    }
                }
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * Porte d'entrée du solveur.
     *
     * @param g la grille à résoudre.
     * @throws RowIndexOutOfBoundsException    voir getValeur()
     * @throws ColumnIndexOutOfBoundsException voir getValeur()
     * @throws UnauthorizedCharacterException  voir getValeur()
     * @throws BadValueException               voir getValeur()
     */
    public static void solve(final Grille g) throws RowIndexOutOfBoundsException, ColumnIndexOutOfBoundsException, UnauthorizedCharacterException, BadValueException {
        int[] plusSur = g.getPlusSur();
        solve(g, plusSur[0], plusSur[1]);
    }
}

package Sudoku.Sudoku;

import Sudoku.Sudoku.Exceptions.BadValueException;
import Sudoku.Sudoku.Exceptions.ColumnIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.RowIndexOutOfBoundsException;
import Sudoku.Sudoku.Exceptions.UnauthorizedCharacterException;

import java.util.ArrayList;

/**
 * Interface d'une grille de sudoku.
 */
abstract class Grille implements IGrille, Cloneable {

    /**
     * Dimension de la grille.
     */
    private final int dimension;

    /**
     * la grille.
     */
    private final char[][] grille;

    /**
     * le nombre de possibilité par cases.
     */
    private final int[][] possibilites;

    /**
     * Caractere possible a mettre dans la grille.
     * pour une grille 9x9 : 1..9
     * pour une grille 16x16: 0..9-a..f
     */
    private final char[] legalChars;


    /**
     * Constructeur qui prends la dimension en arg.
     *
     * @param lc la dimension de la grille
     */
    Grille(final char[] lc) {
        this.dimension = lc.length;
        this.legalChars = lc;
        this.grille = new char[dimension][dimension];
        this.possibilites = new int[dimension][dimension];
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                possibilites[i][j] = 0;
            }

        }
    }

    /**
     * Getter du tableau des possibilités.
     *
     * @return les possibilités pour chaque cellule sous la forme d'un tableau
     */
    public int[][] getPossibilites() {
        return possibilites;
    }

    /**
     * Getter du tableau de caractere.
     *
     * @return les caracteres possible a mettre dans la grille
     */
    public char[] getLegalChars() {
        return legalChars;
    }

    @Override
    public int getDimension() {
        return dimension;
    }

    @Override
    public void setValue(final int x, final int y, final char value)
            throws BadValueException,
            RowIndexOutOfBoundsException,
            ColumnIndexOutOfBoundsException,
            UnauthorizedCharacterException {
        isEnBorne(x, y);
        isLegalChar(value);
        if (isPossible(x, y, value)) {
            grille[x][y] = value;
        } else {
            throw new BadValueException(
                    "la valeur est interdite aux vues des autres "
                            + "valeurs de la grille (valeur = " + value
                            + " at x=" + x + " y=" + y + ")");
        }
    }

    @Override
    public char getValue(final int x, final int y)
            throws RowIndexOutOfBoundsException,
            ColumnIndexOutOfBoundsException {
        isEnBorne(x, y);
        return grille[x][y];
    }

    @Override
    public final boolean complete() {
        for (char[] clist : this.grille) {
            for (char c : clist) {
                if (c == IGrille.EMPTY) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public final boolean isPossible(final int x,
                                    final int y, final char value) {
        if (value == EMPTY) {
            return true;
        }
        return !isInRow(x, value) && !isInCol(y, value)
                && !isInSquare(x, y, value);
    }

    /**
     * Teste il existe déjà la valeur dans la ligne.
     *
     * @param x     la ligne
     * @param value la valeur
     * @return true ou false
     */
    private boolean isInRow(final int x, final char value) {
        for (int i = 0; i < dimension; i++) {
            if (grille[x][i] == value) {
                return true;
            }
        }
        return false;
    }

    /**
     * Teste il existe déjà la valeur dans la colonne.
     *
     * @param y     la colonne
     * @param value la valeur
     * @return true ou false
     */
    private boolean isInCol(final int y, final int value) {
        for (int i = 0; i < dimension; i++) {
            if (grille[i][y] == value) {
                return true;
            }
        }
        return false;
    }

    /**
     * Teste il existe déjà la valeur dans le carré.
     *
     * @param x     la ligne
     * @param y     la colonne
     * @param value la valeur
     * @return true ou false
     */
    private boolean isInSquare(final int x, final int y, final char value) {
        int sqrt = (int) Math.sqrt(dimension);
        int r = x - x % sqrt;
        int c = y - y % sqrt;
        for (int i = r; i < r + sqrt; i++) {
            for (int j = c; j < c + sqrt; j++) {
                if (grille[i][j] == value) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Teste si le caractère est autorisé.
     *
     * @param val le caractère à tester
     * @throws UnauthorizedCharacterException si le caractère ne l'est pas
     */
    private void isLegalChar(final char val) throws
            UnauthorizedCharacterException {
        if (val == '@') {
            return;
        }
        for (char c : legalChars) {
            if (val == c) {
                return;
            }
        }
        throw new UnauthorizedCharacterException(
                "Le caractère n'est pas autorisé dans cette grille. "
                        + "(Caractère : " + val + ")");
    }

    /**
     * Retourne true si la coordonnée est dans la grille.
     *
     * @param x l'abcisse
     * @param y l'ordonnée
     * @throws ColumnIndexOutOfBoundsException si l'indice de la ligne
     *                                         est trop grand ou trop petit
     * @throws RowIndexOutOfBoundsException    si l'indice de la colonne est
     *                                         trop grand ou trop petit
     */
    private void isEnBorne(final int x, final int y) throws
            ColumnIndexOutOfBoundsException, RowIndexOutOfBoundsException {
        if (x < 0 || x > dimension - 1) {
            throw new RowIndexOutOfBoundsException(
                    "La ligne x est en dehors des bornes");
        }
        if (y < 0 || y > dimension - 1) {
            throw new ColumnIndexOutOfBoundsException(
                    "La colonne y est en dehors des bornes");
        }
    }

    /**
     * Rafraichi le tableau du nombre de possibilités.
     *
     * @throws RowIndexOutOfBoundsException    voir getValue()
     * @throws ColumnIndexOutOfBoundsException voir getValue()
     */
    private void calculerPossibilites() throws RowIndexOutOfBoundsException, ColumnIndexOutOfBoundsException {
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                possibilites[i][j] = 0;
                if (getValue(i, j) == EMPTY) {
                    for (char c : legalChars) {
                        if (isPossible(i, j, c)) {
                            possibilites[i][j]++;
                        }
                    }
                } else {
                    possibilites[i][j] = -1;
                }
            }
        }
    }

    /**
     * Rafraichi le nombre de possibilité pour chaque case vide, et
     * renvoie les coordonnées de première cas avec le moins de
     * possibilités.
     *
     * @return un tableau de deux int, le premier étant la colonne
     * et le second la ligne de la première case avec le moins de possibilités
     * @throws RowIndexOutOfBoundsException    voir calculerPossibilite
     * @throws ColumnIndexOutOfBoundsException voir calculerPossibilite
     */
    public int[] getPlusSur() throws RowIndexOutOfBoundsException, ColumnIndexOutOfBoundsException {
        int[] plusSur = new int[2];
        int minPossibilite = Integer.MAX_VALUE;
        calculerPossibilites();
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                if (possibilites[i][j] >= 0 && possibilites[i][j] < minPossibilite) {
                    minPossibilite = possibilites[i][j];
                    plusSur[0] = i;
                    plusSur[1] = j;
                }
            }
        }
        return plusSur;
    }

    /**
     * Renvoie les coordonnées de la première case sur laquelle une seule valeur
     * est possible, et s'il n'y en a pas, renvoie un tableau vide.
     *
     * @return Un tableau de 3 char, le premier étant la ligne de la valeur
     * le second la colonne de la valeur
     * le troisème la valeur à entrer dans la grille.
     */
    public char[] getSeuleValeurPossibleSurUneCase() {
        char[] seuleValeurPossibleSurUneCase = new char[3];
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                if (possibilites[i][j] == 1) {
                    for (char c : getLegalChars()) {
                        if (isPossible(i, j, c)) {
                            seuleValeurPossibleSurUneCase[0] = (char) (i);
                            seuleValeurPossibleSurUneCase[1] = (char) (j);
                            seuleValeurPossibleSurUneCase[2] = c;
                            return seuleValeurPossibleSurUneCase;
                        }
                    }
                }
            }
        }
        return new char[]{};
    }

    /**
     * Renvoie les coordonnées et la valeur d'une valeur s'il existe une ligne qui
     * ne peut pas avoir cette valeur entrée à d'autre coordonnées.
     *
     * @return Un tableau de 3 char, le premier étant la ligne de la valeur
     * le second la colonne de la valeur
     * le troisème la valeur à entrer dans la grille.
     * @throws RowIndexOutOfBoundsException    voir getValue()
     * @throws ColumnIndexOutOfBoundsException voir getValue()
     */
    public char[] getSeulEmplacementPossibleSurLaLignePourUneValeur() throws RowIndexOutOfBoundsException, ColumnIndexOutOfBoundsException {
        char[] seulEmplacementPossibleSurLaLignePourUneValeur = new char[3];
        ArrayList<Character> valeursManquante;
        for (int i = 0; i < dimension; i++) {
            valeursManquante = new ArrayList<Character>() {{
                for (char c : getLegalChars()) {
                    add(c);
                }
            }};
            for (int j = 0; j < dimension; j++) {
                if (getValue(i, j) != EMPTY) {
                    valeursManquante.remove(Character.valueOf(getValue(i, j)));
                }
            }
            for (char manquant : valeursManquante) {
                int nbrEmplacementPossibles = 0;
                for (int j = 0; j < dimension; j++) {
                    if (getValue(i, j) == EMPTY && isPossible(i, j, manquant)) {
                        nbrEmplacementPossibles++;
                        seulEmplacementPossibleSurLaLignePourUneValeur[0] = (char) i;
                        seulEmplacementPossibleSurLaLignePourUneValeur[1] = (char) j;
                        seulEmplacementPossibleSurLaLignePourUneValeur[2] = manquant;
                    }
                }
                if (nbrEmplacementPossibles == 1) {
                    return seulEmplacementPossibleSurLaLignePourUneValeur;
                }
            }
        }
        return new char[]{};
    }

    /**
     * Renvoie les coordonnées et la valeur d'une valeur s'il existe une colonne qui
     * ne peut pas avoir cette valeur entrée à d'autre coordonnées.
     *
     * @return Un tableau de 3 char, le premier étant la ligne de la valeur
     * le second la colonne de la valeur
     * le troisème la valeur à entrer dans la grille.
     * @throws RowIndexOutOfBoundsException    voir getValue()
     * @throws ColumnIndexOutOfBoundsException voir getValue()
     */
    public char[] getSeulEmplacementPossibleSurLaColonnePourUneValeur() throws RowIndexOutOfBoundsException, ColumnIndexOutOfBoundsException {
        char[] seulEmplacementPossibleSurLaColonnePourUneValeur = new char[3];
        ArrayList<Character> valeursManquante;
        for (int i = 0; i < dimension; i++) {
            valeursManquante = new ArrayList<Character>() {{
                for (char c : getLegalChars()) {
                    add(c);
                }
            }};
            for (int j = 0; j < dimension; j++) {
                if (getValue(j, i) != EMPTY) {
                    valeursManquante.remove(Character.valueOf(getValue(j, i)));
                }
            }
            for (char manquant : valeursManquante) {
                int nbrEmplacementPossibles = 0;
                for (int j = 0; j < dimension; j++) {
                    if (getValue(j, i) == EMPTY && isPossible(j, i, manquant)) {
                        nbrEmplacementPossibles++;
                        seulEmplacementPossibleSurLaColonnePourUneValeur[0] = (char) j;
                        seulEmplacementPossibleSurLaColonnePourUneValeur[1] = (char) i;
                        seulEmplacementPossibleSurLaColonnePourUneValeur[2] = manquant;
                    }
                }
                if (nbrEmplacementPossibles == 1) {
                    return seulEmplacementPossibleSurLaColonnePourUneValeur;
                }
            }
        }

        return new char[]{};
    }

    /**
     * Renvoie les coordonnées et la valeur d'une valeur si il existe un bloc qui
     * ne peut pas avoir cette valeur entrée à d'autre coordonnées.
     *
     * @return Un tableau de 3 char, le premier étant la ligne de la valeur
     * le second la colonne de la valeur
     * le troisème la valeur à entrer dans la grille.
     * @throws RowIndexOutOfBoundsException    voir getValue()
     * @throws ColumnIndexOutOfBoundsException voir getValue()
     */
    public char[] getSeulEmplacementPossibleSurLeBlocPourUneValeur() throws RowIndexOutOfBoundsException, ColumnIndexOutOfBoundsException {
        char[] seulEmplacementPossibleSurLeBlocPourUneValeur = new char[3];

        int sqrt = (int) Math.sqrt(dimension);
        ArrayList<Character> valeursManquante;

        for (int i = 0; i < dimension; i = i + 3) {
            for (int j = 0; j < dimension; j = j + 3) {
                int ligne = i + i % sqrt;
                int colonne = j + j % sqrt;
                valeursManquante = new ArrayList<Character>() {{
                    for (char c : getLegalChars()) {
                        add(c);
                    }
                }};
                for (; i < ligne + sqrt; i++) {
                    for (; j < colonne + sqrt; j++) {
                        if (getValue(ligne, colonne) != EMPTY) {
                            valeursManquante.remove(Character.valueOf(getValue(ligne, colonne)));
                        }
                    }
                }
                for (char manquant : valeursManquante) {
                    int nbrEmplacementPossibles = 0;
                    for (; j < ligne + sqrt; j++) {
                        for (; j < colonne + sqrt; j++) {
                            if (isPossible(ligne, colonne, manquant)) {
                                nbrEmplacementPossibles++;
                                seulEmplacementPossibleSurLeBlocPourUneValeur[0] = (char) i;
                                seulEmplacementPossibleSurLeBlocPourUneValeur[1] = (char) j;
                                seulEmplacementPossibleSurLeBlocPourUneValeur[2] = manquant;
                            }
                        }
                    }
                    if (nbrEmplacementPossibles == 1) {
                        return seulEmplacementPossibleSurLeBlocPourUneValeur;
                    }
                }
            }
        }

        return new char[]{};
    }
}
